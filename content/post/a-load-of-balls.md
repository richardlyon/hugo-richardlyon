+++
bigimg = ""
title = "Spheres"
subtitle = "A load of balls."
date = "2015-12-03T16:35:06+01:00"
+++

I'd like to learn to draw. So I've bought Kister's [You Can Draw in 30 Days: The Fun, Easy Way to Learn to Draw in One Month or Less](http://www.amazon.co.uk/You-Can-Draw-Days-Landscapes/dp/0738212415/ref=sr_1_1/276-9340253-6812465?ie=UTF8&qid=1449501526&sr=8-1&keywords=kistler+draw). In it, he asks you to draw a plane, a house, and a bagel before you start the course. Here they are. These are all drawn on an [iPad Pro](https://www.apple.com/ipad-pro/), using an [Apple Pencil](https://www.apple.com/apple-pencil/) and [Procreate](http://procreate.si).

{{< figure src="/img/Day_1_of_30_Initial_Test.jpg" title="Initial test drawing" >}}

Lesson 1 is about the sphere. Here's my first attempt:

{{< figure src="/img/Lesson_1_-_Sphere.jpg" title="The Sphere" >}}

Then he asks you to apply that to drawing an apple:

{{< figure src="/img/Lesson_1_-_Apple.jpg" title="Apple" >}}

Lesson 2 is all about overlapping spheres. The idea here is to pick out "nook and cranny" shading to identify, separate and define the different objects. Here's my first stab.

{{< figure src="/img/Lesson_2_-_Overlapping_Spheres.jpg" title="Overlapping Spheres" >}}

Lesson 3 steps it up to multiple spheres, and introduces perspective. Here, I've used my finger to smooth the shading to create a metallic or glassy finish.

{{< figure src="/img/Lesson_3_-_Advanced_Spheres.jpg" title="Advanced Level Spheres" >}}

In the Lesson 3 challenge, he invites you to have ... fun with spheres. Here's what I came up with. I screwed up a bit and filled the sky black after drawing in it - it's created artifacts that wouldn't have been there if I'd drawn the black sky first. But it's pretty good.

{{< figure src="/img/Lesson_3_-_Bonus_Challenge.jpg" title="Planet Thing" >}}

I'm startled at how quickly these are improving.
