+++
bigimg = "/img/phoenix.jpg"
title = "The case for gas"
subtitle = "Examining the claims for renewable technologies"
date = "2016-12-10T13:01:31+01:00"
+++

**What the plight of ‘The Flight of the Phoenix’ teaches us about current energy priorities.**

Proponents of sustainable energy arrangements (of which I am one) face a difficult challenge: how can we reconcile the fundamental incompatibility of low grade [1], intermittent, unaffordable renewable energy technologies with the requirement of industrial economies for energy sources which are high grade, continuously available, and affordable?

In our largely well meaning efforts to build confidence in alternative energy pathways, to mobilise support, and to accelerate transition, we are inclined to refract the statistics of renewable technologies through glasses of the rosiest hue. Self delusion, however, is not a luxury we can afford.

If the overwhelming consensus of the global community of climate scientists is to be relied on, the earth has already exceeded the safe working limit of the atmosphere to absorb climate change gases. Even if we stopped burning carbon based fuels today, they claim, our climate is on a trajectory with outcomes resembling the plot lines of Hollywood science fiction dystopia.

In the classic 1965 movie ‘The Flight of the Phoenix’, a cargo plane crash lands in the desert. The crew rebuilds a new plane - the Phoenix - from the wreckage of the old one. James Stewart has one engine starter cartridge left. If it doesn’t start the engine, they die. There’s a bit of a row about the best way to go about it. It’s gripping stuff.

So, too, is energy policy in the 21st century. With time running out as our environment degrades, our stock of cheap high grade fuels depletes, and our debt levels soar to unsustainable levels, energy policy has to assemble a new energy system from the wreckage of the old one, and start its engines with our small remaining stock of energy and financial starter cartridges. It’s essential that we don’t waste time, and that we don’t squander our starter cartridges.

Yet when we contaminate energy policy formulation with statistical distortion, we misallocate finite resource. Wasting time and wasting starter cartridges is precisely what we do when we delude ourself, however well meaning our intention.

Take, for example, a recent article by Bloomberg’s ‘New Energy Finance’ ([‘The World Nears Peak Fossil Fuels for Electricity’](http://www.bloomberg.com/news/articles/2016-06-13/we-ve-almost-reached-peak-fossil-fuels-for-electricity), June 13, 2016). BNEF claims to provide ‘unique analysis, tools and data for decision makers driving change in the energy system’, and their analysis is typical of mainstream reporting in the renewable energy sector.

In the article, BNEF presents historical statistics claiming to demonstrate that renewable technology availability (‘capacity factor’) is improving dramatically. It presents future extrapolations which create the impression that marginal construction costs and retail prices of renewable energy are falling below those of thermal generation. On the basis of this analysis, it predicts that ‘There Will Be No Golden Age of Gas’.

A more considered inspection of the statistics appears to suggest precisely the reverse conclusion.

## Going wild

BNEF claim that renewable power plant capacity utilisation improvements is a ‘fast-moving story’ in which capacity factors are ‘going wild’, noting that 50% utilisation has been achieved in the best locations.

Yet the graph supporting the claim reveals a different story. In the last fifteen years, average utilisation has risen only 10 percentage points, is still only 25% and, by 2040, will still be less than 40% on current trends. And, since investment is (presumably) allocated on a least-inefficient-first basis, the expectation that even this unimpressive trend can be maintained is, at best, optimistic.

{{< figure src="/img/wind-tech-efficiency.png" title="Fig 1. Average renewable power plant utilisation has risen only 10 percentage points in the last 15 years, and is still around only 25%. Source: Bloomberg" >}}


## Ugly, ugly math

BNEF claims for falling prices are slightly more complex to deconstruct, but even more misleading. In a chart titled ‘The Beautiful Math of Solar Power’, the logarithm of the price of solar panels is plotted against the logarithm of cumulative generating capacity and a trend line constructed from which, it is claimed, “wind and solar power will be the cheapest forms of producing electricity in most of the world by the 2030s”. This is a critical claim, upon which the probability of renewables displacing gas is highly dependent.

{{< figure src="/img/beautiful-math.png" title="Fig 2. Log-log plot of solar power cost against cumulative capacity. The relationship isn’t demonstrably exponential, the slope is distorted by unrelated early space program costs and recent market oversupply corrections, and log compression conceals significant future timescales and resource implication. Source: Bloomberg." >}}

The so-called ‘log-log’ plot is an analytical device for interpreting phenomena which demonstrate exponential growth. If the phenomenon is exponential, it generates a straight line on a log-log plot. Extrapolation of that line allows accurate estimates of future values to be computed easily.

Unfortunately, a straight line on a log-log plot can also be drawn through the early data of a great many phenomena which are not exponential, and which therefore produces a meaningless extrapolation which significantly overestimates the phenomenon under investigation. Furthermore, the compression and distortion of the scale grossly magnifies the error contribution of early time data and, even under valid conditions, makes interpretation of late time extrapolations highly misleading [2].

The theoretical basis for the claim that the relationship between solar panel cost and cumulative capacity is exponential is the ‘learning’ hypothesis. Renewable energy fuel costs, it is argued, is zero, so cost efficiency is free to improve at a rate described by a learning coefficient: 26% in the Bloomberg article.

Yet the relationship is dominated by factors unrelated to learning. For example. Solar PV originated in the space program. Early solar panel costs were dominated, not by manufacturing costs comparable with today, but by the expense of making them survive being launched into space and subjected to extreme thermal cycling and hard radiation. The cost price reductions since 2008 were caused, not by ‘learning’, but by gross market oversupply corrections. Any straight line coerced into passing through that data is going to have a higher (that is, more flattering) slope.

Worse, the zero fuel cost assumption is wrong. A renewable system requires a duplicate backup system to maintain energy supplies during synchronous failure. That has been provided so far ‘for free’ by legacy thermal generation capacity and would have to be explicitly costed if renewables made significant penetration.

And the math (properly understood) is ugly, not beautiful. An order of magnitude increase of a small thing (the left of the graph) is easy. An order of magnitude increase of a large thing (the right of the graph) - particularly a thing that consumes large and largely invisible quantities of energy, resources, and money - is very much harder. That innocuous-looking final two centimetres of x-axis, projected back into the time domain, implies exponentially increasing consumption of everything from rare earth elements to energy (the continued unconstrained supply of which is by no means certain) over several decades of effort [3].

A more accurate interpretation of the graph would be: “An order of magnitude increase in physical and energy resource drawdown, which is likely to take decades to implement, will yield at most a 26% cost reduction”. The temptation to obscure messy physical reality behind tidy log-log abstraction, and leave the energy policy maker to draw the wrong conclusion, is clear.

## If we’ve one cartridge left, how should we use it? The case for gas

Taken together, the claim that gas is obviated by renewable technologies that, despite decades of effort, only delivers 25% of nominal capacity, and will take decades and colossal quantities of material and financial resource to deliver necessary cost reductions, starts to look unconvincing.

Meanwhile, something has to keep the lights on during the synchronised failure of renewable power plants sitting idle during those long, regionally extensive periods of cold, dark, windless conditions that so frequently accompany winter high pressure weather systems in the northern industrial regions. I’d argue that anyone who suggests that a complex industrial system can be made to run for extended periods on batteries hasn’t thought long enough about what is happening, thermodynamically speaking, at the working end of a Pratt & Whitney 747 turbofan.

At the scales implied in the analysis, a renewable energy based energy system will require a 100% duplicate thermal power generation system, maintained continuously on short-latency standby. The cost of this is not accommodated in Bloomberg’s ‘beautiful’ (sic.) log-log learning curve plot. We can’t burn coal, oil supply growth is proving remarkably difficult to sustain, and the nuclear industry presents its own rich crop of Hollywood dystopia plot lines. There seem to be few viable alternatives to gas.

More problematic, it’s not entirely clear that we have sufficient financial and energy resources to pursue dead-end science projects and white elephants. A view is emerging [4] that the current forms of renewable technology are so inefficient that they will never achieve the levels required to sustain economic activity, and that the public debt currently financing deployment subsidies should be reallocated toward R&D of (currently unidentified) technologies with potential efficiencies of at least an order of magnitude higher. Back to the Phoenix: if current renewable technology is inadequate, and adequate technology has not been identified, then gas is all we’ve got.

Most problematic is the non sequitur at the heart of the renewable technology proposition, so fundamental that we don’t speak of it. Renewable energy sources are so diffuse (in a thermodynamic sense) that they must be concentrated before they can perform useful work. The concentration devices (a.k.a. “solar PV cells”, “wind turbines”, etc.) are complex products of the global industrial manufacturing system. The global industrial manufacturing system - the collection of mining, refining, heat forming, assembly, transportation, operation, and maintenance activities, together with its human element - is so energy intensive that it can only run on hydrocarbon.

Expecting a diffuse energy source to power the manufacture of the devices required to concentrate it to the levels required to power their own manufacture is like expecting a ball to roll uphill. Yet that (unconscious) circular reasoning lies at the heart of the the renewable energy proposition [5]. Since it can’t, the only way the devices will get built is with a hydrocarbon energy source i.e gas.

—

If the situation is uncertain, we should avoid at all costs making it more so by overstating and misrepresenting renewable energy’s progress and potential, in our well intentioned and undeniably necessary efforts to shift the direction of energy policy. Optimism is necessary. But it’s not sufficient. Energy policy must be based on hard fact, not sales brochures.

Post script: Private comments have noted the fundamental tension established in this post between the observation that we can’t burn any more hydrocarbon, and the claim that we must burn gas. For the sake of personal disclosure, my view is that this tension can’t be resolved. However, whichever path we take, getting from here to there will, paradoxically, require energy. Gas, in my view, is the least damaging source of it.



### Notes

[1] ‘Grade’ here has a particular meaning in thermodynamics, referring to energy’s concentration and therefore capacity to do work. Fossil fuels are intensely concentrated and can drive industrial processes directly. Solar and wind energy sources are highly diffuse and require extremely energy intensive (i.e. hydrocarbon powered) industrial processes to manufacture the necessary concentration devices.

[2] For a tour of the potholes of power law phenomena analysis visited in the Bloomberg article, see Shalizi, C. 2007. 'So You Think You Have a Power Law — Well Isn't That Special?’

[3] For a review of the unintuitive consequences of renewable technology’s implied exponential resource demand growth, see Lyon, R. 2016. 'The lethal exponential function’. Energy Literacy.

[4] See, for example, Helm, Dieter. 2015. 'The Carbon Crunch'. Yale University Press.

[5] This fatal non sequitur in the logic of renewable energy transition - the inability of a diffuse energy source to power the manufacturing process required to concentrate it - is one that few have yet grasped. A plethora of studies exist claiming to demonstrate the energy payback of renewable devices, all of which start with the unstated assumption: “Assume the existence of a global industrial manufacturing system”.
